<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title><?=$title?></title>
		<link rel="stylesheet" href="assets/text.css?<?=stat('assets/text.css')['mtime']?>">
	</head>
	<body>
		<header>
            <i><?=$header?></i>
		</header>
		<content>
            <?=$content?>
            <?=$basket?>
		</content>
		<footer>
            <p><i><?=$footer?></i></p>
		</footer>
	</body>
</html>	