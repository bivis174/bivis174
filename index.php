<?php
error_reporting(E_All);
ini_set('display_errors', 'on');

$title="Мой Магазин";
$header="";
$footer="";
$errors="";
$content='<h1>Магазин</h1>';
$summ=0;
$arrshop=[];

//Путём небольшой магии получаем массив магазина
$arrshop=[
    '123'=>['name'=>'Помидор', 'price'=>141, 'quantity'=>45],
    '205'=>['name'=>'Огурец','price'=>60, 'quantity'=>54],
    '306'=>['name'=>'Картошка', 'price'=>20, 'quantity'=>160],
    '615'=>['name'=>'Чеснок','price'=>450, 'quantity'=>30],
    '766'=>['name'=>'Кабачок','price'=>45, 'quantity'=>70]
];

session_start();
//session_destroy();
require_once 'class/Shop.php';
require_once 'class/Basket.php';

if(!empty($_POST["requantity"]))
    if(!is_numeric($_POST["requantity"]["quantity"]))
    {
        $_SESSION['errors']="<b>Ошибка!!!</b> Строка преобразованна в число<br>";
        preg_match_all('#\d#',$_POST["requantity"]["quantity"],$match);
        $post=implode('',$match[0]);
        //если $post пустой забираем значение из сессии;
        if($post=="")
        {
            $post = $_SESSION['shop'][$_POST["requantity"]["id"]];
            $_SESSION['errors']="<b>Ошибка!!!</b> Вы ввели не числовое значение<br>";
        }
        $_POST["requantity"]["quantity"]=$post;
    }

//жмём на кнопку и добавляем сессию
if(!empty($_POST['add']))
{
    $id=$_POST['add'];
	if(!empty($_SESSION['shop'][$id]))
	{
		$_SESSION['shop'][$id]++;
	}
	else
		$_SESSION['shop'][$id]=1;
}

//меняем колличество
if(!empty($_POST["requantity"]))
{
	$limit=Limit();
	$_SESSION['shop'][$_POST["requantity"]["id"]]=$limit;
}
	
//Удаляем если нажата кнопка (удалить);
if(!empty($_POST["del"]))
    unset($_SESSION['shop'][$_POST["del"]]);
//Удаляем если достигнута нижняя граница;
if(!empty($_POST["requantity"]))
    if($_POST["requantity"]["quantity"]<=0)
        unset($_SESSION['shop'][$_POST["requantity"]["id"]]);

function Limit()
{
	global $arrshop;
	$id=$_POST["requantity"]["id"];
	if($arrshop[$id]['quantity']<=$_POST["requantity"]["quantity"])
		return $arrshop[$id]['quantity'];
	return 	$_POST["requantity"]["quantity"];	
}

//Изменяем таблицу магазина Вычитаем корзину;
if(!empty($_SESSION['shop']))
{
    //Удаляем если магазин $arrshop изменился
     foreach($_SESSION['shop'] as $id=>$elem)
        if(!isset($arrshop[$id]))
            unset($_SESSION['shop'][$id]);


	foreach($_SESSION['shop'] as $id=>$elem)
		//проверка что колличество элементов не больше лимита;
		if($elem>=$arrshop[$id]['quantity'])
		{	
			$_SESSION['shop'][$id]=$arrshop[$id]['quantity'];
			$arrshop[$id]['quantity']=0;
		}
		else
			$arrshop[$id]['quantity']-=$elem;
}

//Находим общую стоимость
foreach($_SESSION['shop'] as $id=>$elem)
	$summ+=$elem*$arrshop[$id]['price'];

if(!empty($_POST))
{
    header("Location: $_SERVER[REQUEST_URI]");
    die();//останавливаем выполнение скрипта
}

//создание информации об ошибке
if(!empty($_SESSION['errors']))
    $errors=$_SESSION['errors'];

//наполняем элемент
$content.=Shop::getShops();
if(!empty($_SESSION['shop']))
{
    $basketElem=Basket::getBasket();
    ob_start();
    include 'elems/basket.php';
    $basket = ob_get_clean();
}
include 'elems/layout.php';
//после того как страница прогрузится удаляем пользовательскую информацию о ошибках из сессии
unset($_SESSION['errors']);
?>